// Shared code needed by all three pages.
let insideTable;

class Run {
    constructor(startLocation,arrayLocation,destinationLocation,startDate){
        this._startLocation = startLocation;
        this._destinationLocation = destinationLocation;
        this._arrayLocation = arrayLocation;
        this._startDate = startDate;
        this._endDate;
        this._runName;
        this._duration;
        this._distance;
        this._speed;
    }
    get runName(){
        return this._runName;
    }
    get startLocation(){
        return this._startLocation;
    }
    get destinationLocation(){
        return this._destinationLocation;
    }
    get arrayLocation(){
        return this._arrayLocation;
    }
    get startDate(){
        return this._startDate;
    }
    get endDate(){
        return this._endDate;
    }
    get duration(){
        return this._duration;
    }
    get distance(){
        return this._distance;
    }
    get speed(){
        return this._speed;
    }
}
// Prefix to use for Local Storage.  You may change this.
var APP_PREFIX = "monash.mcd4290.runChallengeApp";
var ATTEMPT_STATE ="Attempt_State";
// Array of saved Run objects.
var savedRuns = [];
var runNames = [];

function setAttemptState(state){
    attemptState = state;
}

function setReattemptRunIndex(index){
    reattemptRunIndex = index;
}


