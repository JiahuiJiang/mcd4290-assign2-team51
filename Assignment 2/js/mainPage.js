// Code for the main app page (Past Runs list).

// The following is sample code to demonstrate navigation.
// You need not use it for final app.

function viewRun(runIndex)
{
    // Save the desired run to local storage so it can be accessed from View Run page.
    localStorage.setItem(APP_PREFIX + "-selectedRun", runIndex);
    // ... and load the View Run page.
    location.href = 'viewRun.html';
}


function showRunList(){
    let insideTable = [];
    let savedRun = [];
    if (typeof(Storage) !== "undefined")
    {
        // Retrieving Previous Runs
        savedRun = JSON.parse(localStorage.getItem(APP_PREFIX))
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
    for(let i = 0; i < savedRun.length; i++){
        insideTable += "<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRun(" + i + ");\"><span class=\"mdl-list__item-primary-content\"><span>" + savedRun[i]._runName + "</span><span class=\"mdl-list__item-sub-title\">" + savedRun[i]._startDate + "</span></span></li>"
                        
    }
    let outputArea = document.getElementById("runsList")
    outputArea.innerHTML += insideTable
}

showRunList()