// Code for the View Run page.

// The following is sample code to demonstrate navigation.
// You need not use it for final app.
var map, marker1, marker2, id;
var y = 0
let crd,currentLngLat,currentLat,currentLng,currentLocationAccurate
let distance1, distance2
var runIndex = localStorage.getItem(APP_PREFIX + "-selectedRun");
var Allruns = JSON.parse(localStorage.getItem(APP_PREFIX))
var selectedPastRun = Allruns[runIndex]
mapboxgl.accessToken ='pk.eyJ1IjoiZ3JvdXA1MSIsImEiOiJjazlkeHFxZmwwNzhxM21wOGlpdjFldnlhIn0.ZpsrySTE82l0wtRb_q5Yyw'


if (runIndex !== null)
{
    // If a run index was specified, show name in header bar title. This
    // is just to demonstrate navigation.  You should set the page header bar
    // title to an appropriate description of the run being displayed.
    var runNames = [];
    let savedRunData = [];
    if (typeof(Storage) !== "undefined")
    {
        // Retrieving Previous Runs
        savedRunData = JSON.parse(localStorage.getItem(APP_PREFIX))
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
    for(let j = 0; j<savedRunData.length;j++) {
        runNames.push(savedRunData[j]._runName)
    }
    document.getElementById("headerBarTitle").textContent = runNames[runIndex];
}


function viewPastRun(){
    map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: selectedPastRun._startLocation
    });
    
    marker1 = new mapboxgl.Marker().setLngLat(selectedPastRun._startLocation).addTo(map);
}

function showRoute(){
    map.addSource('route', {
        'type': 'geojson',
        'data': {
            'type': 'Feature',
            'properties': {},
            'geometry': {
                'type': 'LineString',
                'coordinates': selectedPastRun._arrayLocation
            }
        }
    });
        
    map.addLayer({
        'id': 'route',
        'type': 'line',
        'source': 'route',
        'layout': {
            'line-join': 'round',
            'line-cap': 'round'
        },

        'paint': {
            'line-color': '#888',
            'line-width': 8
        }
    });
    
    marker2 = new mapboxgl.Marker().setLngLat(selectedPastRun._arrayLocation[1]).addTo(map);
}

function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
}


function reattempt(){
    if(distance2<10){
        let reattemptIndex = {
            attemptState:1,
            reattemptRunIndex:runIndex
        }
        let string = JSON.stringify(reattemptIndex)
        localStorage.setItem(ATTEMPT_STATE,string)
        location.href = 'newRun.html'
    } else {
        alert("You are too far from the starting location!")
    }
}

function deleteRun(){
    let runDeleted = Allruns.splice(runIndex,1)
    let string = JSON.stringify(Allruns);
    localStorage.setItem(APP_PREFIX, string);
    location.href = 'index.html'
}


function getDistance(lat1,lng1,lat2,lng2){
    let radLat1 = lat1*Math.PI/180;
    let radLat2 = lat2*Math.PI/180;
    let radLng1 = lng1*Math.PI/180;
    let radLng2 = lng1*Math.PI/180;
    let a = radLat1 - radLat2;
    let b = radLng1 - radLng2;
    let s = 2* 6378.137 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));// cal
    s = Math.round(s*1000) // km to m
    
    return s;
}


function success(position) {
    crd = position.coords;
    currentLngLat = [position.coords.longitude, position.coords.latitude];        
    currentLat = position.coords.latitude;
    currentLng = position.coords.longitude;
    currentLocationAccurate = crd.accuracy;
    
    //Distance from destination location
    distance1 = getDistance(currentLng,currentLat,selectedPastRun._destinationLocation[0],selectedPastRun._destinationLocation[1])
    //Distance from starting location
    distance2 = getDistance(currentLng,currentLat,selectedPastRun._startLocation[0],selectedPastRun._startLocation[1]);
    /*
    if (y === 0){
        selectedPastRun._endDate = new Date();
    }
    if(distance1<10){     
        selectedPastRun._destinationLocation = [currentLng,currentLat];
        selectedPastRun._duration = selectedPastRun._endDate - selectedPastRun._startDate
        alert("You have finished your run!")
        y = 1
    }
    
    if(distance2<10){
        x = 1
    }   
    */
}



/*
function assignRunName(){
    nicknameRef = "";
    nicknameRef = document.getElementById("nickname").value;
    selectedPastRun._runName = nicknameRef;
} 
*/



/*
function saveData(){
    // Saving the run List
    assignRunName()
    if (typeof(Storage) !== "undefined")
    {
        // Retrieving Previous Runs
        let savedRun = [];
        savedRun = JSON.parse(localStorage.getItem(APP_PREFIX))
        if (savedRun === null){
            savedRun = [];
        }
        // Updating the list of runs and saving it.
        savedRun.push(selectedPastRun)
        let string = JSON.stringify(savedRun);
        localStorage.setItem(APP_PREFIX, string);
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
    
    /*
    if (typeof(Storage) !== "undefined")
    {
        // TODO: Retrieve the stored JSON string and parse
        //       to a variable called deckObject.
        //       Use this to initialise an new instance of
        //       the Deck class.
        insideTable = localStorage.getItem("insideTable")
    }
    if (typeof(Storage) !== "undefined")
    {
        insideTable += "<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRun(" + runIndex + ");\"><span class=\"mdl-list__item-primary-content\"><span>" + runNames + "</span><span class=\"mdl-list__item-sub-title\">" + startTime + "info</span></span></li>"
        localStorage.setItem(APP_PREFIX, insideTable);
    }
    
    // Now clear memory.
    selectedPastRun = null;
    nicknameRef = null;
    navigator.geolocation.clearWatch(id);
    
    location.href = 'index.html'
}
*/





options = {
    enableHighAccuracy: true,
    timeout: Infinity,
    maximumAge: 0
};

id = navigator.geolocation.watchPosition(success, error, options)
viewPastRun()
let outputArea = document.getElementById("info")
let duration = selectedPastRun._duration
let distance = selectedPastRun._distance
let speed = selectedPastRun._speed
outputArea.innerHTML += "The distance of the run is " + distance + ".<br>"
if(duration === undefined || speed === undefined ){
    outputArea.innerHTML += "You haven't attempted the run yet.<br>"
}else{
    outputArea.innerHTML += "The duration of the run is " + duration + ".<br>"
    outputArea.innerHTML += "The speed of the run is " + speed + ".<br>"
}
