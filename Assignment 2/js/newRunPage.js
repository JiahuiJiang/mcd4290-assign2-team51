// Code for the Measure Run page.

//make these variable to the global variable
let id,target, options;
let currentLat;
let currentLng;
let currentLocationAccurate;
let newPosition;
let distance,map;
let startTime;
let nickname,nicknameRef;
let runInstance = [];
let marker1, marker2, y;
let crd, currentLngLat;
let status = 0;
let index;
mapboxgl.accessToken ='pk.eyJ1IjoiZ3JvdXA1MSIsImEiOiJjazlkeHFxZmwwNzhxM21wOGlpdjFldnlhIn0.ZpsrySTE82l0wtRb_q5Yyw';


let state = JSON.parse(localStorage.getItem(ATTEMPT_STATE))
if (state === null){
    state = [];
}
let attemptState = state.attemptState
let reattemptRunIndex = state.reattemptRunIndex

if(attemptState === 1){
    let button1 = document.getElementById('button1')
    button1.disabled = true;
    let button2 = document.getElementById('button2')
    button2.disabled = false;
    
    if (typeof(Storage) !== "undefined")
    {
        // Retrieving Previous Runs
        let savedRun = [];
        savedRun = JSON.parse(localStorage.getItem(APP_PREFIX))
        if (savedRun === null){
            savedRun = [];
        }
        // Updating the list of runs and saving it.
        runInstance = savedRun[reattemptRunIndex]
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
    state.attemptState = 0
    let string = JSON.stringify(state);
    localStorage.setItem(ATTEMPT_STATE, string);
    index = 1;
    
} else if(attemptState === 0){
    let button1 = document.getElementById('button1')
    button1.disabled = false;
    let button2 = document.getElementById('button2')
    button2.disabled = true;
}

function showRoute(){
    map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': runInstance._arrayLocation
                }
            }
        });

        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },

            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        });

        marker2 = new mapboxgl.Marker().setLngLat(runInstance._arrayLocation[1]).addTo(map);
}

function randomDestination (LngLatOfCurrentLocation){
    nowLng = LngLatOfCurrentLocation[0];
    nowLat = LngLatOfCurrentLocation[1];
    
    //decide add or subtract
    let addOrSubtract1 = Math.round(Math.random());
    let addOrSubtract2 = Math.round(Math.random());
    
    //create new LNG and LAT
    if(addOrSubtract1 === 0){
        nowLng += Math.random()*0.001;
    }else{
        nowLng -= Math.random()*0.001;
    }
    
    if(addOrSubtract2 === 0){
        nowLat -= Math.random()*0.001;
    }else{
        nowLat += Math.random()*0.001;
    }

    
    let randomPosition = [nowLng,nowLat];
    
    
    return randomPosition;
}



//calculate distance between two LatLng;
function getDistance(lat1,lng1,lat2,lng2){
    let radLat1 = lat1*Math.PI/180;
    let radLat2 = lat2*Math.PI/180;
    let radLng1 = lng1*Math.PI/180;
    let radLng2 = lng1*Math.PI/180;
    let a = radLat1 - radLat2;
    let b = radLng1 - radLng2;
    let s = 2* 6378.137 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));// cal
    s = Math.round(s*1000) // km to m
    
    return s;
}



function success(position) {
    crd = position.coords;
    /*
    if (target.latitude === crd.latitude && target.longitude === crd.longitude) {
        console.log('Congratulations, you reached the target');
        navigator.geolocation.clearWatch(id);

    }
    */
    currentLngLat = [position.coords.longitude, position.coords.latitude];
            
    currentLat = position.coords.latitude;
    currentLng = position.coords.longitude;
    currentLocationAccurate = crd.accuracy;
    
    
    if(status == 0){
        map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: currentLngLat
        }); 
        status = 1;
    }
    
    if (y === 0){
        runInstance._endDate = new Date();
    }
    if(distance<10){     
        runInstance._destinationLocation = [currentLng,currentLat];
        runInstance._duration = runInstance._endDate - runInstance._startDate
        alert("You have finished your run!")
        y = 1
    }
    
    //take a marker at current position
    marker1 = new mapboxgl.Marker().setLngLat(currentLngLat).addTo(map);
}



function getNewPosition(){
    if (currentLocationAccurate<=20){
        newPosition = randomDestination([currentLng,currentLat]);
        distance = getDistance(currentLng,currentLat,newPosition[0],newPosition[1]);
        while(distance <=60 || distance >=150){
            newPosition = randomDestination([currentLng,currentLat]);
            distance = getDistance(currentLng,currentLat,newPosition[0],newPosition[1]);
        }
    
        map.addSource('route', {
            'type': 'geojson',
            'data': {
                'type': 'Feature',
                'properties': {},
                'geometry': {
                    'type': 'LineString',
                    'coordinates': [
                        [currentLng,currentLat],
                        newPosition
                    ]
                }
            }
        });
        
        map.addLayer({
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },

            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        });

        marker2 = new mapboxgl.Marker().setLngLat(newPosition).addTo(map);
        document.getElementById("distanceref").innerHTML = '<br>Distance from destination to current position is: ' + distance;
    } else{
        alert("Your device location accuracy is too low. Please try again!")
    }
}



function displayAccurate(){
    document.getElementById("accurateRef").innerHTML = '<br>Current accurate is: ' + currentLocationAccurate ;
}
        


function error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
}

/*
target = {
latitude : 0,
longitude: 0 
};
*/



options = {
    enableHighAccuracy: true,
    timeout: Infinity,
    maximumAge: 0
};



function beginRun(){
    startTime = new Date();
    runInstance = new Run([currentLng,currentLat],[[currentLng,currentLat],newPosition],newPosition,startTime);
    runInstance._distance = distance
}

function assignRunName(){
    nicknameRef = "";
    nicknameRef = document.getElementById("nickname").value;
    runInstance._runName = nicknameRef;
}


function saveData(){
    // Saving the run List
    runInstance._speed = distance/runInstance._duration
    assignRunName()
    if (typeof(Storage) !== "undefined")
    {
        // Retrieving Previous Runs
        let savedRun = [];
        savedRun = JSON.parse(localStorage.getItem(APP_PREFIX))
        if (savedRun === null){
            savedRun = [];
        }
        // Updating the list of runs and saving it.
        savedRun.push(runInstance)
        let string = JSON.stringify(savedRun);
        localStorage.setItem(APP_PREFIX, string);
    }
    else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
    
    /*
    if (typeof(Storage) !== "undefined")
    {
        // TODO: Retrieve the stored JSON string and parse
        //       to a variable called deckObject.
        //       Use this to initialise an new instance of
        //       the Deck class.
        insideTable = localStorage.getItem("insideTable")
    }
    if (typeof(Storage) !== "undefined")
    {
        insideTable += "<li class=\"mdl-list__item mdl-list__item--two-line\" onclick=\"viewRun(" + runIndex + ");\"><span class=\"mdl-list__item-primary-content\"><span>" + runNames + "</span><span class=\"mdl-list__item-sub-title\">" + startTime + "info</span></span></li>"
        localStorage.setItem(APP_PREFIX, insideTable);
    }
    */
    
    // Now clear memory.
    runInstance = null;
    nicknameRef = null;
    navigator.geolocation.clearWatch(id);
    
    location.href = 'index.html'
}



function panToTheCurrentLocation(){
    map.panTo([currentLng,currentLat]);
}


/*
function clearUp(){
    runInstance = null;
    newPosition = null;
    startTime = null;
    map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: currentLngLat
    });
    marker1 = new mapboxgl.Marker().setLngLat(currentLngLat).addTo(map)
}
*/

console.log(new Date()); 

id = navigator.geolocation.watchPosition(success, error, options);
